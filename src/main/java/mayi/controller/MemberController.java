package mayi.controller;

import mayi.domain.Order;
import mayi.domain.User;
import mayi.mapper.HomeMapper;
import mayi.mapper.OrderMapper;
import mayi.mapper.UserMapper;
import mayi.support.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@RequestMapping("member")
public class MemberController extends BaseController {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private HomeMapper homeMapper;
    @Autowired
    private UserMapper userMapper;

    /**
     *
     * @return
     */
    @RequestMapping("index")
    public String index(Model model) {
        model.addAttribute("msg", session.getAttribute("msg"));
        session.removeAttribute("msg");
        if (null == frontUser()) return "redirect:/login";
        User user = userMapper.selectByPrimaryKey(frontUser().getId());
        model.addAttribute(user);
        Order order = new Order();
        order.setUserid(frontUser().getId());
        List<Order> orderList = orderMapper.select(order);
        model.addAttribute("orderList",orderList);
        return "member";
    }

    /**
     * 更新个人信息
     *
     * @param user
     * @return
     */
    @RequestMapping("update")
    public String update(User user, Model model, MultipartFile img) {
        String file = saveFile(img);
        user.setIcon(file);
        user.setId(frontUser().getId());
        userMapper.updateByPrimaryKeySelective(user);
        session.setAttribute("msg", "修改成功");
        return "redirect:index";
    }

    /**
     * 修改密码
     *
     * @return
     */
    @RequestMapping("modfy")
    public String modfy(Model model, String password, String newpassword, String repassword) {
        if (!newpassword.equals(repassword)) {
            session.setAttribute("msg", "两次密码不一样");
            return "redirect:index";
        }
        User user = frontUser();
        if (!user.getPassword().equals(password)) {
            session.setAttribute("msg", "原密码错误");
            return "redirect:index";
        }
        user.setPassword(newpassword);
        userMapper.updateByPrimaryKeySelective(user);
        session.setAttribute("msg", "修改成功");
        return "redirect:index";
    }


}
