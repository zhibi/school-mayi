package mayi.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mayi.domain.Order;
import mayi.mapper.OrderMapper;
import mayi.support.BaseController;
import mayi.support.pojo.OrderPo;
import mayi.support.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tk.mybatis.mapper.entity.Example;

@RequestMapping("admin/order")
@Controller
public class AdminOrderController extends BaseController {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 评论列表
     *
     * @param model
     * @param pageNum
     * @return
     */
    @RequestMapping("list")
    public String list(Model model, @RequestParam(defaultValue = "1") Integer pageNum) {
        Example example = new Example(Order.class);
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<OrderPo> pageInfo = new PageInfo<OrderPo>(orderMapper.selectPoByExample(example), 5);
        model.addAttribute(pageInfo);
        model.addAttribute("url", request.getRequestURI() + "?" + ParamUtils.params2String(request));
        return "admin/order/list";
    }

}
