package mayi.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mayi.domain.Notice;
import mayi.mapper.NoticeMapper;
import mayi.support.BaseController;
import mayi.support.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

@Controller
@RequestMapping("admin/notice")
public class AdminNoticeController extends BaseController {


    @Autowired
    private NoticeMapper noticeMapper;

    /**
     * 列表
     *
     * @param notice
     * @param pageNum
     * @param model
     * @return
     */
    @RequestMapping("list")
    public String list(Notice notice, @RequestParam(defaultValue = "1") Integer pageNum, Model model) {
        Example example = new Example(Notice.class);
        Example.Criteria criteria = example.createCriteria();
        if (notice.getTitle() != null) {
            criteria.andLike("title", "%" + notice.getTitle() + "%");
        }
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<Notice> pageInfo = new PageInfo<>(noticeMapper.selectByExample(example), 5);
        model.addAttribute(pageInfo);
        model.addAttribute("url", request.getRequestURI() + "?" + ParamUtils.params2String(request));
        return "admin/notice/list";
    }


    /**
     * 详情
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("detail/{id}")
    public String detail(@PathVariable Integer id, Model model) {
        Notice notice = noticeMapper.selectByPrimaryKey(id);
        model.addAttribute(notice);
        return "admin/notice/detail";
    }

    /**
     * 修改信息
     *
     * @param notice
     * @return
     */
    @RequestMapping("update")
    public String update(Notice notice) {
        noticeMapper.updateByPrimaryKeySelective(notice);
        return refresh();
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("del/{id}")
    public String del(@PathVariable Integer id) {
        noticeMapper.deleteByPrimaryKey(id);
        return refresh();
    }

    @GetMapping("add")
    public String add() {
        return "admin/notice/add";
    }

    @RequestMapping("add")
    public String add(Notice notice) {
        notice.setAddtime(new Date());
        noticeMapper.insertSelective(notice);
        return "redirect:list";
    }
}
