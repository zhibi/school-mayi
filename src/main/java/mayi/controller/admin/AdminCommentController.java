package mayi.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mayi.mapper.CommentMapper;
import mayi.support.BaseController;
import mayi.support.pojo.CommentPo;
import mayi.support.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("admin/comment")
@Controller
public class AdminCommentController extends BaseController {

    @Autowired
    private CommentMapper commentMapper;

    /**
     * 评论列表
     *
     * @param model
     * @param pageNum
     * @return
     */
    @RequestMapping("list")
    public String list(Model model, @RequestParam(defaultValue = "1") Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        PageInfo<CommentPo> pageInfo = new PageInfo<CommentPo>(commentMapper.selectPo(), 5);
        model.addAttribute(pageInfo);
        model.addAttribute("url", request.getRequestURI() + "?" + ParamUtils.params2String(request));
        return "admin/comment/list";
    }


    /**
     * 删除
     *
     * @param id
     * @return
     */
    @RequestMapping("del/{id}")
    public String del(@PathVariable Integer id) {
        commentMapper.deleteByPrimaryKey(id);
        return refresh();
    }

}
