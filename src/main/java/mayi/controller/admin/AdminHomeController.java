package mayi.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mayi.domain.Home;
import mayi.mapper.HomeMapper;
import mayi.support.BaseController;
import mayi.support.util.ParamUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import tk.mybatis.mapper.entity.Example;

@RequestMapping("admin/home")
@Controller
public class AdminHomeController extends BaseController {

    @Autowired
    private HomeMapper homeMapper;

    /**
     * 列表
     *
     * @param home
     * @param pageNum
     * @param model
     * @return
     */
    @RequestMapping("list")
    public String list(Home home, @RequestParam(defaultValue = "1") Integer pageNum, Model model) {
        Example example = new Example(Home.class);
        Example.Criteria criteria = example.createCriteria();
        if (home.getName() != null) {
            criteria.andCondition("name like '%" + home.getName() + "%'");
        }

        PageHelper.startPage(pageNum, pageSize);
        PageInfo<Home> pageInfo = new PageInfo<>(homeMapper.selectByExample(example), 5);
        model.addAttribute(pageInfo);
        model.addAttribute("url", request.getRequestURI() + "?" + ParamUtils.params2String(request));

        return "admin/home/list";
    }


    @RequestMapping("del/{id}")
    public String del(@PathVariable Integer id) {
        homeMapper.deleteByPrimaryKey(id);
        return refresh();
    }
}
