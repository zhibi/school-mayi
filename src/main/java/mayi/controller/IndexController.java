package mayi.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import mayi.domain.Home;
import mayi.domain.Notice;
import mayi.domain.User;
import mayi.mapper.CommentMapper;
import mayi.mapper.HomeMapper;
import mayi.mapper.NoticeMapper;
import mayi.mapper.UserMapper;
import mayi.service.UserService;
import mayi.support.BaseController;
import mayi.support.pojo.CommentPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class IndexController extends BaseController {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private HomeMapper homeMapper;
    @Autowired
    private NoticeMapper noticeMapper;
    @Autowired
    private CommentMapper commentMapper;

    @RequestMapping({"/"})
    public String index(Model model) {
        model.addAttribute("msg", session.getAttribute("msg"));
        session.removeAttribute("msg");
        Example example = new Example(Home.class);
        PageHelper.startPage(1, 15);
        List<Home> list = homeMapper.selectByExample(example);
        model.addAttribute("list", list);

        if(frontUser()!=null){
            example = new Example(Home.class);
            PageHelper.startPage(1, 15);
            example.createCriteria().andCondition("address like '%"+frontUser().getAddress()+"%'");
            List<Home> myList = homeMapper.selectByExample(example);
            model.addAttribute("myList", myList);
        }

        return "home";
    }


    /**
     * 列表页
     *
     * @param model
     * @param pageNum
     * @param name
     * @return
     */
    @RequestMapping({"index"})
    public String index(Model model, @RequestParam(defaultValue = "1") Integer pageNum, @RequestParam(defaultValue = "") String name,
                        @RequestParam(defaultValue = "") String price,
                        @RequestParam(defaultValue = "") String ress) {
        model.addAttribute("msg", session.getAttribute("msg"));
        session.removeAttribute("msg");



        Example example = new Example(Home.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andCondition("(name like '%" + name + "%' or address like '%"+name+"%')");
        if(!price.isEmpty()){
            String[] split = price.split("_");
            criteria.andGreaterThanOrEqualTo("price",split[0]);
            criteria.andLessThanOrEqualTo("price",split[1]);
        }
        if(!ress.isEmpty()){
            criteria.andCondition("( address like '%"+ress+"%')");
        }


        PageHelper.startPage(pageNum, 10);
        PageInfo<Home> pageInfo = new PageInfo<>(homeMapper.selectByExample(example), 5);
        setModelAttribute(model, pageInfo);
        //公告列表
        List<Notice> noticeList = noticeMapper.selectAll();
        model.addAttribute("noticeList", noticeList);
        model.addAttribute("price", price);
        return "index";
    }

    /**
     * 房子详情
     *
     * @return
     */
    @RequestMapping("detail/{id}")
    public String detail(@PathVariable Integer id, Model model) {
        Home home = homeMapper.selectByPrimaryKey(id);
        model.addAttribute(home);
        List<CommentPo> commentList = commentMapper.selectPoByHome(id);

        model.addAttribute("commentList", commentList);
        return "detail";
    }


    @GetMapping("login")
    public String login() {
        return "login";
    }

    @PostMapping("login")
    public String login(String phone, String password, Model model, HttpSession session) {
        User temp = userService.selectByPhone(phone);
        if (temp == null || !temp.getPassword().equals(password)) {
            model.addAttribute("msg", "用户名或密码错误");
            return "login";
        }
        session.setAttribute("sessionUser", temp);
        return "redirect:index";
    }


    @GetMapping("register")
    public String register() {
        return "register";
    }

    @PostMapping("register")
    public String register(String phone, String password, Model model) {
        User temp = userService.selectByPhone(phone);
        if (temp != null) {
            model.addAttribute("msg", "该手机号已经被注册");
            return "register";
        }

        User user = new User();
        user.setAddtime(new Date());
        user.setPhone(phone);
        user.setPassword(password);
        user.setType("default");
        user.setName(phone);
        userMapper.insertSelective(user);
        return "redirect:index";
    }


    @RequestMapping("logout")
    public String logout() {
        session.removeAttribute("sessionUser");
        return "redirect:index";
    }

    /**
     * 公告详情
     *
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("notice/{id}")
    public String notice(@PathVariable Integer id, Model model) {
        Notice notice = noticeMapper.selectByPrimaryKey(id);
        model.addAttribute(notice);
        return "notice";
    }
}
