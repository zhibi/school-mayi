package mayi.controller;

import mayi.domain.Home;
import mayi.domain.Order;
import mayi.mapper.HomeMapper;
import mayi.mapper.OrderMapper;
import mayi.support.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
@RequestMapping("order")
public class OrderController extends BaseController {

    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private HomeMapper homeMapper;

    /**
     * 到下订单页面
     *
     * @param order
     * @return
     */
    @RequestMapping("index")
    public String index(Order order, Model model) {
        if(null == frontUser()) return "redirect:/login";
        order.setAddtime(new Date());
        order.setUserid(frontUser().getId());
        order.setStatus("未支付");
        int day = differentDays(order.getStarttime(), order.getEndtime());
        order.setDay(day + "");
        Home home = homeMapper.selectByPrimaryKey(order.getHomeid());
        order.setPrice(home.getPrice() * day);
        orderMapper.insertSelective(order);
        model.addAttribute(order);
        model.addAttribute(home);
        return "order";
    }

    @RequestMapping("pay")
    public String pay(Order order) {
        order.setStatus("已支付");
        orderMapper.updateByPrimaryKeySelective(order);
        session.setAttribute("msg","支付成功");
        return "redirect:/index";
    }

    /**
     * 计算两个日期时间差
     *
     * @param date1
     * @param date2
     * @return
     */
    private int differentDays(Date date1, Date date2) {
        int days = (int) ((date2.getTime() - date1.getTime()) / (1000 * 3600 * 24));
        return days;
    }
}
