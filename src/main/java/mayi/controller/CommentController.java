package mayi.controller;

import mayi.domain.Comment;
import mayi.mapper.CommentMapper;
import mayi.support.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@RequestMapping("comment")
@Controller
public class CommentController extends BaseController {

    @Autowired
    private CommentMapper commentMapper;

    /**
     * 评论
     * @return
     */
    @RequestMapping("send")
    public String send(String  content ,Integer homeid){
        if (null == frontUser()) return "redirect:/login";
        Comment comment = new Comment();
        comment.setAddtime(new Date());
        comment.setContent(content);
        comment.setHomeid(homeid);
        comment.setUserid(frontUser().getId());
        commentMapper.insertSelective(comment);
        //session.setAttribute("msg","评论成功");
        return refresh();
    }
}
