package mayi.domain;

import java.io.Serializable;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


/**
* The class create by 执笔 on generate.
* The class generate create by home
*/
@Table(name="home")
public class Home implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Double price;

    private String address;

    private String type;//房屋信息

    private String descript;//房屋介绍

    private String content;//图片

    private String other;//其他

    private String status;

    private String traffic;//交通
    private String tag;
    private Integer commentnum;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getCommentnum() {
        return commentnum;
    }

    public void setCommentnum(Integer commentnum) {
        this.commentnum = commentnum;
    }

    public String getTraffic() {
        return traffic;
    }

    public void setTraffic(String traffic) {
        this.traffic = traffic;
    }

    public Integer getId(){
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice(){
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAddress(){
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public String getType(){
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getDescript(){
        return descript;
    }

    public void setDescript(String descript) {
        this.descript = descript;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOther(){
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getStatus(){
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Home{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                ", descript='" + descript + '\'' +
                ", content='" + content + '\'' +
                ", other='" + other + '\'' +
                ", status='" + status + '\'' +
                ", traffic='" + traffic + '\'' +
                ", tag='" + tag + '\'' +
                ", commentnum=" + commentnum +
                '}';
    }
}