package mayi.service.impl;

import mayi.domain.User;
import mayi.mapper.UserMapper;
import mayi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;

	@Override
	public User login(String username, String password) {
		User user = new User();
		user.setName(username);
		user.setPassword(password);
		return userMapper.selectOne(user);
	}

	@Override
	public User selectByEmail(String email) {
		User user = new User();
		user.setEmail(email);
		return userMapper.selectOne(user);
	}

	@Override
	public User selectByName(String username) {
		User user = new User();
		user.setName(username);
		return userMapper.selectOne(user);
	}

	@Override
	public User selectByPhone(String phone) {
		User user = new User();
		user.setPhone(phone);
		return userMapper.selectOne(user);
	}
}