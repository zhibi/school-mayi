package mayi.mapper;

import mayi.domain.Comment;
import mayi.support.pojo.CommentPo;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
* Created by 执笔 on generate.
*/
public interface CommentMapper extends Mapper<Comment>{

    List<CommentPo> selectPo();

    List<CommentPo> selectPoByHome(Integer id);
}
