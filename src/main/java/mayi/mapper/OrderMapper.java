package mayi.mapper;

import mayi.support.pojo.OrderPo;
import tk.mybatis.mapper.common.Mapper;
import mayi.domain.Order;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
* Created by 执笔 on generate.
*/
public interface OrderMapper extends Mapper<Order>{

    List<OrderPo> selectPoByExample(Example example);

}
