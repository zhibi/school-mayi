package mayi.support.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class JSONUtils {

    public static Map<String, Object> jsonToMap(String json) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(json, Map.class);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * 将对象转换为json
     *
     * @param obj
     * @return
     * @Author 执笔
     * @Date 2016年10月26日下午3:17:11
     */
    public static String objToJson(Object obj) {
        if (null == obj) return null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    /**
     * 将json转换为object
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T jsonToObj(String json, Class<T> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);//不对应字段的时候
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * @param map
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T mapToObj(Map map, Class<T> clazz) {
        if (null == map) return null;
        String json = objToJson(map);
        return jsonToObj(json, clazz);
    }

    /**
     * 对象转换成map
     *
     * @param object
     * @return
     */
    public static Map<String, Object> objToMap(Object object) {
        if (object == null) return new HashMap<>();
        String json = objToJson(object);
        return jsonToMap(json);
    }
}
