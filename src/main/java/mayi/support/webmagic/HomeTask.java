package mayi.support.webmagic;

import mayi.domain.Comment;
import mayi.domain.Home;
import mayi.mapper.CommentMapper;
import mayi.mapper.HomeMapper;
import mayi.support.util.FileUtils;
import us.codecraft.webmagic.ResultItems;
import us.codecraft.webmagic.Task;
import us.codecraft.webmagic.pipeline.Pipeline;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class HomeTask implements Pipeline {

    private static String filePath = "E:\\ProjectSpace\\School_Work\\mayi\\src\\main\\webapp\\";

    private HomeMapper homeMapper;
    private CommentMapper commentMapper;

    public HomeTask(HomeMapper homeMapper, CommentMapper commentMapper) {
        this.homeMapper = homeMapper;
        this.commentMapper = commentMapper;
    }

    @Override
    public void process(ResultItems resultItems, Task task) {
        Object id = resultItems.get("id");
        Object name = resultItems.get("name");
        Object address = resultItems.get("address");
        Object path = resultItems.get("path");
        Object phone = resultItems.get("phone");
        Object price = resultItems.get("price");
        Object type = resultItems.get("type");
        Object descript = resultItems.get("descript");
        Object other = resultItems.get("other");
        Object traffic = resultItems.get("traffic");
        Object tag = resultItems.get("tag");
        List<String> comments = resultItems.get("comment");

        //已经有的忽略
        if (homeMapper.selectByPrimaryKey(Integer.parseInt(id.toString())) != null) return;

        //没有手机号的忽略
        if (null == phone || phone.toString().equals("")) return;

        Home home = new Home();
        home.setId(Integer.parseInt(id.toString()));
        home.setName(name.toString());
        home.setAddress(address.toString());
        home.setCommentnum(0);
        home.setType(type.toString());

        home.setContent("");
        String[] list = path.toString().replace("[", "").replace("]", "").split(",");
        for (String url : list) {
            url = url.replace("140x88c.jpg", "420x264c.jpg");
            String imgPath = "\\data\\" + UUID.randomUUID().toString() + ".jpg";
            FileUtils.downloadPicture(url, filePath + imgPath);
            home.setContent(home.getContent() + "#" + imgPath);
        }


        home.setStatus("wait");
        home.setPrice(Double.parseDouble(price.toString()));
        home.setDescript((String) descript.toString());
        home.setOther((String) other.toString());
        home.setPhone((String) phone.toString());
        home.setTag((String) tag.toString());
        home.setTraffic((String) traffic.toString());
        for(String comment : comments){
            Comment c = new Comment();
            c.setHomeid(Integer.parseInt(id.toString()));
            c.setAddtime(new Date());
            c.setContent(comment);
            commentMapper.insertSelective(c);
        }
        homeMapper.insertSelective(home);



    }
}
