package mayi.support.webmagic;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

import java.util.List;

/**
 * 网络爬虫
 * http://www.mayi.com/beijing/
 */
public class HomeProcessor implements PageProcessor {
    // 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
    private Site site = Site.me().setRetryTimes(3).setSleepTime(1000);

    @Override
    public void process(Page page) {
        if (page.getUrl().regex("http://www.youtx.com/beijing/page\\d").match() || page.getUrl().toString().equals("http://www.youtx.com/beijing/")) {
           //分页
            List<String> href = page.getHtml().$(".pagebar-right a", "href").all();
            page.addTargetRequests(href);
            //房屋
            List<String> homes = page.getHtml().$(".housePrice", "id").all();
            for(String id : homes){
                String url = "http://www.youtx.com/room/"+id.replace("houseprice","");
                page.addTargetRequest(url);
            }
            page.setSkip(true);
        } else if (page.getUrl().regex("http://www.youtx.com/room/\\d").match()) {

            page.putField("id",page.getHtml().$("input[name='houseid']","value"));
           page.putField("name",page.getHtml().$(".sec-1 h2","text"));
            page.putField("address",page.getHtml().$(".sec-2 i","title"));
            page.putField("path",page.getHtml().$(".dsmallpic img","src").all());
            page.putField("phone",page.getHtml().$(".callphone  span","text"));
            page.putField("price",page.getHtml().$("#PriceNum_Value2  span","text"));
            page.putField("type",page.getHtml().$(".housemessage  .lists"));
            page.putField("descript",page.getHtml().$(".houseintroduce  .lists"));
            page.putField("other",page.getHtml().$(".others  .lists"));
            page.putField("traffic",page.getHtml().$(".traffic  .lists"));
            page.putField("tag",page.getHtml().$(".dpictags   li").all());

            //评论
            page.putField("comment", page.getHtml().$("div .text","text").all());
        }
    }

    @Override
    public Site getSite() {
        return site;
    }

   public static void main(String[] args) {

        Spider.create(new HomeProcessor())
                .addUrl("http://www.youtx.com/beijing/")
                //开启5个线程抓取
                .thread(5)
                //启动爬虫
                .run();
    }
}
