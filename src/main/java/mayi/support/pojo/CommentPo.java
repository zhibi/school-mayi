package mayi.support.pojo;


import mayi.domain.Comment;

public class CommentPo extends Comment {

    private String username;
    private String icon;

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
