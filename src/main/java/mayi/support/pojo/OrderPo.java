package mayi.support.pojo;

import mayi.domain.Order;

public class OrderPo extends Order {

    private String home;
    private String userName;

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
