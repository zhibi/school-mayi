package mayi.support;

import mayi.mapper.CommentMapper;
import mayi.mapper.HomeMapper;
import mayi.support.webmagic.HomeProcessor;
import mayi.support.webmagic.HomeTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import us.codecraft.webmagic.Spider;

@Component
public class Task {

    @Autowired
    private HomeMapper homeMapper;
    @Autowired
    private CommentMapper commentMapper;

    @Scheduled(cron = "0 07 23 * * ?")
    public void webmagic() {
        System.out.println("开启爬虫");
        Spider.create(new HomeProcessor())
                .addUrl("http://www.youtx.com/beijing/")
                //开启5个线程抓取
                .thread(5)
                //启动爬虫
                .addPipeline(new HomeTask(homeMapper,commentMapper))
                .run();
    }


}
