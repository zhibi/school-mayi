<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>

<div class="sidebar" id="sidebar">
    <ul class="nav nav-list">


        <li>
            <a href="/admin/user/list"><i class="icon-user"></i>用户管理</a>
        </li>

        <li>
            <a href="/admin/home/list"><i class="icon-list"></i>房屋管理</a>
        </li>
        <li>
            <a href="/admin/notice/list"><i class="icon-list"></i>公告管理</a>
        </li>
        <li>
            <a href="/admin/notice/add"><i class="icon-plus"></i>添加公告</a>
        </li>

        <li>
            <a href="/admin/comment/list"><i class="icon-list"></i>评论管理</a>
        </li>

        <li>
            <a href="/admin/order/list"><i class="icon-list"></i>订单管理</a>
        </li>

    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left"
           data-icon2="icon-double-angle-right"></i>
    </div>
</div>
